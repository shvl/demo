```
> git clone https://github.com/cloudfoundry/uaa.git
> cd uaa 
> vim Dockerfile

FROM java

RUN mkdir /src
WORKDIR /src

ENTRYPOINT ./gradlew :cloudfoundry-identity-uaa:war
```

```
docker run -it -v /tmp/uaa:/src tmpbuilder
```

```
> vim Dockerfile

FROM tomcat:8-jre8

RUN rm -rf $CATALINA_HOME/webapps/*
COPY uaa.yml /uaa/uaa.yml
COPY cloudfoundry-identity-uaa-3.7.4.war $CATALINA_HOME/webapps/ROOT.war
ENV UAA_CONFIG_PATH /uaa

CMD ["catalina.sh", "run"]
```

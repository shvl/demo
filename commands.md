##Создание базы данных

Запускаем mysql. Указываем пароль администратора и рабочую директорию для хранения файлов
```
> docker run -d -v ~/p/demo/mysql/data:/var/lib/mysql -eMYSQL_ROOT_PASSWORD=secret --name oauth-mysql mysql:5.7.15
1a678aed05821e611787e9e0eea5094e81d8e5e8c78c39465244f88aa68d6d31
```

Проверяем что контейнер запущен
```
> docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
1a678aed0582        mysql:5.7.15        "docker-entrypoint.sh"   4 minutes ago       Up 4 minutes        3306/tcp            oauth-mysql
```

Зайдем внутрь контейнера

```
>docker exec -it oauth-mysql bash
root@1a678aed0582:/#
```

```
>mysql -uroot -psecret
....
mysql>
```

Создаем базу данных для oauth
```
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.02 sec)

mysql> create database oauth; 
Query OK, 1 row affected (0.01 sec)
```

выходим из контейнера CTRL+D

## Запускаем Oauth из готового образа

Запускаем контейнер связывая его с контейнером базы данных. Подкладываем конфигурационный файл.
```
docker run -d --name oauth --link oauth-mysql:oauth-mysql -v ~/p/demo/oauth/config:/uaa -p 8082:8080 shvl/oauth:0.0.1
```

## Запуск Rest api на dotnet

````
> docker run -d --name blog-mongo -v ~/p/demo/mongodb/data:/data/db mongo:3.0.12
```

Запускаем rest api
```
docker run -d --name blog-api -p 5000:80 --link oauth:oauth --link blog-mongo:blog-mongo -e "DATABASE_NAME=blog" -e"DATABASE_URL=mongodb://blog-mongo:27017" -e"EnvironmentName=prod" -e"OAUTH_CLIENT=blog" -e"OAUTH_SECRET=blogsecret12" -e"OAUTH_URL=http://oauth:8080/" -e"EnvironmentName:prod" shvl/api:0.0.1
```

Запускаем приложение nodejs  
```
docker run -d --name blog-web -p 3000:3000 shvl/web:0.0.1
```

Запускаем nginx чтобы проксировать часть запросов на /api, а остальные на web
```
docker run -d --name blog-proxy --link blog-api:blog-api --link blog-web:blog-web -v ~/p/demo/nginx:/etc/nginx/conf.d:ro -p3001:80 nginx:1.11.4-alpine
```


import React, {Component} from 'react';
import { connect } from 'react-redux';
import logo from './logo.png';
import Auth from './auth';

class Navigator extends Component{
    render (){
        const user = this.props.user;
        return (
            <div className="blog-masthead">
                <div className="container">
                    <nav className="navbar blog-nav">
                        <a className="navbar-brand" href="/">Blog</a>
                        <ul className="nav navbar-nav">
                            <li className="nav-item active">
                                <a className="nav-link" href="/">Home</a>
                            </li>
                            <Auth user={user} />
                        </ul>
                    </nav>
                </div>
                <div className="logo-container">
                    <img src={logo} />
                </div>
            </div>
        )
    }
}

const getProps = (state) =>{
    return {
        user: state.app.user
    }
}

export default connect(getProps)(Navigator)
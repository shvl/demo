import React, {Component} from 'react';

class Auth extends Component{
    render(){
        const user = this.props.user;
        if (user)
            return (
                <div className="pull-md-right">
                    <li className="nav-item">
                        <span className="nav-link">Hello {user.given_name || user.email}</span>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/new-post">New Post</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="/sign-out">Sign Out</a>
                    </li>
                </div>
            )
        return (
            <div className="pull-md-right">
                <li className="nav-item">
                    <a className="nav-link" href="/sign-in">Sign In</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="/sign-up">Sign Up</a>
                </li>
            </div>
        )
                                
    }
}

export default Auth;
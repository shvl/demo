import React, { Component } from 'react';
import 'whatwg-fetch';
import PostExcerpt from './postExcerpt';

class Category extends Component {

    constructor(props){
        super(props)
        this.state = { 
            posts: [] 
        };
    };

    componentDidMount(){
        fetch('/api/blog/posts/')
            .then(( response ) => response.json())
            .then(( posts ) => this.setState({ posts }));
    }

    render(){
        const posts = this.state.posts;
        return (

            <div className="row">
                <div className="col-md-12 blog-main">
                    {
                        posts.map( (post, index) => <PostExcerpt key={ index } post={ post }/>)
                    }
                </div>
            </div>
        )
    }
}

export default Category; 
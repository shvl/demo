import React, { Component } from 'react';
import { Link } from 'react-router';
import moment from 'moment';

class Excerpt extends Component {
    render(){
        const { title, excerpt, image, slug, date, user } = this.props.post;
        return (
            <div className="blog-post">
                <header className="text-center">
                    <h2 className="blog-post-title">
                        <Link to={`/post/${slug}`}>{title}</Link>
                    </h2>
                </header>
                <p className="blog-post-meta">
                    {moment(date).format('LLL')} by <b>{user}</b>
                </p>
                <div dangerouslySetInnerHTML={{__html: excerpt}}>
                </div>
                <Link className="btn" to={`/post/${slug}`}>Read More...</Link>
                <hr />
            </div>
        )
    }
}
export default Excerpt;
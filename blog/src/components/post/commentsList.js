import React, { Component } from 'react';
import { connect } from 'react-redux'; 
import Comment from './comment';
import {postDispatcher} from '../../dispatchers/post';
import 'whatwg-fetch';

class CommentsList extends Component{
    constructor(props){
        super(props);
        this.state = {
            post: this.props.post,
            comments: [],
            user: this.props.user
        }
    }

    componentDidMount(){
        fetch(`/api/blog/posts/${this.state.post.slug}/comments`)
            .then((response) => {
                return response.json()
            })
            .then(comments => this.setState({comments}));
    }

    onAddCommentClick(event){
        event.preventDefault();
        const user = this.state.user;
        fetch(`/api/blog/posts/${this.state.post.slug}/comments`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.access_token}`,
            },
            body: JSON.stringify({
                comment: this.refs.comment.value
            })
        })
        .then(response => {
            if (response.status !== 200)
                return;
            return response.json();
        })
        .then((comment) => this.setState(Object.assign({}, this.state, {comments: this.state.comments.concat(comment)})));
    }

    render() {
        const {comments, user} = this.state;
        return (
            <section>
                <div>
                    <h2>Comments</h2>
                </div>
                <div>
                    {
                        comments.map((comment, key) => <Comment key={key} comment={comment} />)
                    }
                </div>
                {
                    (() =>{
                        if (user)
                            return (
                                <div>
                                    <hr />
                                    <div className="well">
                                        <h4>Leave a Comment:</h4>
                                        <form role="form">
                                            <div className="form-group">
                                                <textarea className="form-control" rows="3" ref="comment" placeholder="Your Comment"></textarea>
                                            </div>
                                            <button type="submit" className="btn btn-default" onClick={this.onAddCommentClick.bind(this)}>Submit</button>
                                        </form>
                                    </div>
                                </div>
                            )
                    })()
                }
            </section>
        )
    }
}

const getProps = (state) =>{
    return {
        user: state.app.user
    }
}

export default connect(getProps)(CommentsList);
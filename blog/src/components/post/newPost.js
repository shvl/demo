import React, {Component} from 'react';
import Message from '../message';
import { connect } from 'react-redux';
import appDispatcher from '../../dispatchers/app';

class NewPost extends Component{

    constructor(props){
        super(props);
        this.state = {
        }
    }

    onCreateClick(event){
        event.preventDefault();
        fetch('/api/blog/posts', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.props.user.access_token}`,
            },
            body: JSON.stringify({
                title: this.refs.titleInput.value,
                body: this.refs.bodyInput.value
            })
        })
        .then((response) => {
            if (response.status !== 200)
                this.setState(Object.assign({}, this.state, {
                    status: response.status,
                    errorMessage: response.statusText
                }));
            return response.json();
        })
        .then(() => this.props.goHome());
    }

    render(){
        const {errorMessage, status} = this.state;

        return (
            <form>
                <Message message={errorMessage||"Password is invalid"} type="danger" visible={!!status}/>
                <div className="form-group">
                    <label htmlFor="titleInput">Title</label>
                    <input type="text" ref="titleInput" className="form-control" placeholder="Title" />
                </div>                
                <div className="form-group">
                    <label>Post</label>
                    <textarea ref="bodyInput" className="form-control" placeholder="Your Text" />
                </div>                
                <button type="submit" className="btn btn-default" onClick={this.onCreateClick.bind(this)}>Create</button>
            </form>
        )
    }
}

const getProps = (state) =>{
  return {
      user: state.app.user
  };
};

const getDispatchers = (dispatch) =>{
    return {
        goHome: appDispatcher.goHome.bind(null, dispatch)
    }
}

export default connect(getProps, getDispatchers)(NewPost);
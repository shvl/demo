import React, {Component} from 'react';
import avatar from './noavatar.svg';
import moment from 'moment';
import './comment.css'

class Comment extends Component{
    render() {
        const {user, date, comment} = this.props.comment;

        return (
            <div className="media comment">
                <a className="media-left" href="#">
                    <img className="media-object img-circle" src={avatar} alt="avatar" />
                </a>
                <div className="media-body">
                    <h5 className="media-heading pull-left">{user}</h5>
                    <div className="pull-left"><i>{moment(date).format('LLL')}</i></div>
                    <div>
                        {comment}
                    </div>
                </div>
            </div>
        )
    }
}

export default Comment;
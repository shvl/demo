import React, { Component } from 'react';
import { connect } from 'react-redux'; 
import Comment from './comment';
import {postDispatcher} from '../../dispatchers/post';
import 'whatwg-fetch';

class CommentsList extends Component{
    onAddPostClick(event){
        event.preventDefault();
        const user = this.state.user;
        fetch(`/api/blog/posts/`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.access_token}`,
            },
            body: JSON.stringify({
                title: this.refs.titleInput.value,
                slug: this.refs.slugInput.value,
                body: this.refs.bodyInput.value,
            })
        })
    }

    render() {
        const {comments, user} = this.state;
        return (
            <div>
                <hr />
                <div className="well">
                    <h4>Create New Post:</h4>
                    <form role="form">
                        <div className="form-group">
                            <label htmlFor="titleInput">Title</label>
                            <input type="text" ref="titleInput" className="form-control" placeholder="Title" />
                        </div>  
                        <div className="form-group">
                            <label htmlFor="slugInput">Slug</label>
                            <input type="text" ref="slugInput" className="form-control" placeholder="Slug" />
                        </div>  
                        <div className="form-group">
                            <textarea className="form-control" rows="3" ref="bodyInput" placeholder="Text of Your Post"></textarea>
                        </div>
                        <button type="submit" className="btn btn-default" onClick={this.onAddPostClick.bind(this)}>Submit</button>
                    </form>
                </div>
            </div>
        )
    }
}

const getProps = (state) =>{
    return {
        user: state.app.user
    }
}

export default connect(getProps)(CommentsList);
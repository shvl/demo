import React, {Component} from 'react';
import CommentsList from './commentsList';
import moment from 'moment';
import 'whatwg-fetch';

class Post extends Component{
    constructor(props){
        super(props);
        this.state = {
            post: null,
            slug: this.props.params.slug
        }
    }

    componentDidMount(){
        fetch(`/api/blog/posts/${this.state.slug}`)
            .then((response) => {
                if (response.status === 404)
                    return this.setState({notFound: true});

                return response.json()
            })
            .then(post => this.setState({post}))
        fetch(`/api/blog/posts/${this.state.slug}/comments`)
            .then((response) => {
                return response.json()
            })
            .then(comments => this.setState({comments}));
    }

    render() {
        const {post, notFound} = this.state;
        if (!post)
            return null;

        const {title, body, date, user, image} = post;
        return (
            <div className="row">
                <div className="col-md-12 blog-main">
                    <div className="blog-post">
                        <header className="text-center">
                            <h2 className="blog-post-title">{title}</h2>
                        </header>
                        <p className="blog-post-meta">
                            {moment(date).format('LLL')} by <b>{user}</b>
                        </p>
                        <div dangerouslySetInnerHTML={{__html: body}}>
                        </div>
                    </div>
                    <hr />
                    <CommentsList post={post}/>
                </div>
            </div>
        )
    }
}

export default Post;
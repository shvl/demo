import React, { Component } from 'react';

class Message extends Component {

    render(){
        const {type, message, visible} = this.props;
        const visibility = visible?'':'hidden';
        return (
            <div className={`alert alert-${type} ${visibility}`} role="alert" >
            {message}
            </div>
        )
    }
}

export default Message; 
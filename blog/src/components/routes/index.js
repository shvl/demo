import React, { Component } from 'react';
import Category from '../category';
import Post from '../post';
import NewPost from '../post/newPost';
import { SignIn, SignUp, SignOut} from '../auth';
import NotFound from '../notFound';
import { Router, Route, browserHistory } from 'react-router';

class Routes extends Component {
  render() {
    return (
        <Router history={browserHistory}>
            <Route path="/" component={Category}/>
            <Route path="sign-in" component={SignIn}/>
            <Route path="sign-up" component={SignUp}/>
            <Route path="sign-out" component={SignOut}/>
            <Route path="/new-post" component={NewPost}/>
            <Route path="/post/:slug" component={Post}/>
            <Route path="/*" component={NotFound}/>
            <Route path=":slug" component={Post}/>
            <Route path="*" component={NotFound}/>
        </Router>
      );
  }
}

export default Routes;

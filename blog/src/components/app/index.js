import React, { Component } from 'react';
import Navigator from '../navigator';
import Routes from '../routes';
import './app.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navigator />
        <div className="blog-header" />
        <div className="container">
          <Routes />
        </div>
      </div>
    );
  }
}

export default App;

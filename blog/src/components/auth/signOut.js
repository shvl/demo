import React, {Component} from 'react';
import Message from '../message';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import appDispatcher from '../../dispatchers/app';

class SignOut extends Component{
    componentDidMount(){
        this.props.setUser({user: null});
    }

    render(){
        return (
            <h1>Goodbye</h1>
        )
    }
}

const getDispatchers = (dispatch) =>{
    return {
        setUser: appDispatcher.setUser.bind(null, dispatch)
    }
}

export default connect(null, getDispatchers)(SignOut);
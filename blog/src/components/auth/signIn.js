import React, {Component} from 'react';
import Message from '../message';
import { connect } from 'react-redux';
import appDispatcher from '../../dispatchers/app';

class SignIn extends Component{

    constructor(props){
        super(props);
        this.state = {
            pending: false,
            status: null,
            errorMessage: null
        }
    }

    onSignInClick(event){
        event.preventDefault();
        fetch('/api/user/sign-in', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic YmxvZzpibG9nc2VjcmV0MTI=',
            },
            body: JSON.stringify({
                email: this.refs.emailInput.value,
                password: this.refs.passwordInput.value,
            })
        })
        .then((response) => {
            if (response.status !== 200)
                this.setState(Object.assign({}, this.state, {
                    status: response.status,
                    errorMessage: response.statusText
                }));
            return response.json();
        })
        .then((user) => this.props.setUser({user}));
    }

    render(){
        const onSignInClick = this.onSignInClick.bind(this);
        const {errorMessage, status} = this.state;

        return (
            <form>
                <Message message={errorMessage||"Password is invalid"} type="danger" visible={!!status}/>
                <div className="form-group">
                    <label htmlFor="emailInput">Email address</label>
                    <input type="email" ref="emailInput" className="form-control" placeholder="Email" />
                </div>                
                <div className="form-group">
                    <label htmlFor="passwordInput">Password</label>
                    <input type="password" ref="passwordInput" className="form-control" placeholder="Password" />
                </div>                
                <button type="submit" className="btn btn-default" onClick={this.onSignInClick.bind(this)}>SignIn</button>
            </form>
        )
    }
}

const getProps = (state) =>{
  return {
  };
};

const getDispatchers = (dispatch) =>{
    return {
        setUser: appDispatcher.setUser.bind(null, dispatch)
    }
}

export default connect(getProps, getDispatchers)(SignIn);
import React, {Component} from 'react';
import { connect } from 'react-redux';
import appDispatcher from '../../dispatchers/app';

class SignUp extends Component{
    onSignUpClick(event){
        event.preventDefault();
        fetch('/api/user/sign-up', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic YmxvZzpibG9nc2VjcmV0MTI=',
            },
            body: JSON.stringify({
                email: this.refs.emailInput.value,
                givenName: this.refs.givenName.value,
                familyName: this.refs.familyName.value,
                password: this.refs.passwordInput.value,
            })
        })
        .then((response) => {
            if (response.status !== 200)
                this.setState(Object.assign({}, this.state, {
                    status: response.status,
                    errorMessage: response.statusText
                }));
            return response.json();
        })
        .then((user) => this.props.setUser({user}));
    }

    render(){
        return (
            <div>
                <div className="form-group">
                    <label htmlFor="emailInput">Email address</label>
                    <input type="email" ref="emailInput" className="form-control" placeholder="Email" />
                </div>                
                <div className="form-group">
                    <label htmlFor="givenName">First Name</label>
                    <input type="text" ref="givenName" className="form-control" placeholder="First Name" />
                </div>                
                <div className="form-group">
                    <label htmlFor="familyName">Last Name</label>
                    <input type="text" ref="familyName" className="form-control" placeholder="Last Name" />
                </div>                
                <div className="form-group">
                    <label htmlFor="passwordInput">Password</label>
                    <input type="password" ref="passwordInput" className="form-control" placeholder="Password" />
                </div>                
                <button type="submit" className="btn btn-default" onClick={this.onSignUpClick.bind(this)}>SignUp</button>
            </div>
        )
    }
}

const getDispatchers = (dispatch) =>{
    return {
        setUser: appDispatcher.setUser.bind(null, dispatch)
    }
}

export default connect(null, getDispatchers)(SignUp);
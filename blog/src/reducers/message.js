export default (defaultState) => {
  return (state=defaultState, action) => {
    switch (action.type) {
    case 'SHOW_MESSAGE':
      return Object.assign({}, state, {type: action.type, message: action.message, visible: true});
    case 'SHOW_MESSAGE':
      return Object.assign({}, state, {type: action.type, message: action.message, visible: false});
    default:
      return state;
    }
  };
};
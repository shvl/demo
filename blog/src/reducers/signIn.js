export default (defaultState) => {
  return (state=defaultState, action) => {
    switch (action.type) {
    case 'SIGN_IN':
      return Object.assign({}, state, {showLogin: true, loading: false});
    default:
      return state;
    }
  };
};
import {combineReducers} from 'redux';
import signInReducer from './signIn';
import messageReducer from './message';
import appReducer from './app';

export default function(defaultStore){
  return combineReducers({
    app: appReducer(defaultStore.app),
    signIn: signInReducer(defaultStore.signIn),
    message: messageReducer(defaultStore.message)
  });
}
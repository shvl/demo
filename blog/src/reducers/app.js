export default (defaultState) => {
  return (state=defaultState, action) => {
    switch (action.type) {
    case 'SET_USER':
      return Object.assign({}, state, {user: action.user});
    case 'GO_HOME':
      return Object.assign({}, state);
    default:
      return state;
    }
  };
};
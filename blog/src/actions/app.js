import {browserhistory} from 'react-router';

const setUser = ({user}) => {
    return {
        type: 'SET_USER',
        user
    };
};

const goHome = ({user}) => {
    return {
        type: 'GO_HOME'
    };
};

export default {
  setUser,
  goHome
};
import actions from '../actions/app';
import { browserHistory } from 'react-router';

const setUser = (dispatch, {user}) => {
  localStorage.setItem('user', JSON.stringify(user));
  dispatch(actions.setUser({user}));
  browserHistory.push('/');
};

const goHome = (dispatch) => {
  browserHistory.push('/');
};

export default {
  setUser,
  goHome
};
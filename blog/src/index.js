import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import './index.css';
import {createStore, compose} from 'redux';
import {Provider} from 'react-redux';
import 'remote-redux-devtools';
import reducers from './reducers';

export default function configureStore(initialState) {
  const enhancer = compose(
    typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ? window.devToolsExtension() : f => f
  );
  // Note: passing enhancer as last argument requires redux@>=3.1.0
  return createStore(reducers(initialState), initialState, enhancer);
}

const user = JSON.parse(localStorage.getItem('user')); 

let store = configureStore({
    app: {
      user
    },
    signIn: {},
    message: {}
});

ReactDOM.render(
  (
    <Provider store={store}>
      <App />
    </Provider>
  ),
  document.getElementById('root')
);

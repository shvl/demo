#!/bin/bash -e

dbUrl=`echo $DATABASE_URL | sed -e 's|\/|\\\/|g'`
sed -i -e "s/%%DATABASE_NAME%%/$DATABASE_NAME/g" /app/appsettings.json
sed -i -e "s/%%DATABASE_URL%%/$dbUrl/g" /app/appsettings.json


oauthUrl=`echo $OAUTH_URL | sed -e 's|\/|\\\/|g'`
sed -i -e "s/%%OAUTH_CLIENT%%/$OAUTH_CLIENT/g" /app/appsettings.json
sed -i -e "s/%%OAUTH_SECRET%%/$OAUTH_SECRET/g" /app/appsettings.json
sed -i -e "s/%%OAUTH_URL%%/$oauthUrl/g" /app/appsettings.json

dotnet run --server.urls http://*:80
#!/bin/bash

docker stop asprest
docker rm asprest
docker run -d --name asprest -p 5001:80 --link blogMongo:blogMongo \
    -e"DATABASE_NAME=blog" \
    -e"DATABASE_URL=mongodb://blogMongo:27017" \
    -e"OAUTH_CLIENT=blog" \
    -e"OAUTH_SECRET=blogsecret12" \
    -e"OAUTH_URL=http://138.68.88.21:8082/" \
    -e"EnvironmentName:prod" shvl/api:0.0.1 

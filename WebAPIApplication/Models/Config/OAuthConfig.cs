public class IdentityConfig
{

    public string Client { get; set; }
    public string Secret { get; set; }
    public string Url { get; set; }
}
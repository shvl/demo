using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
 
namespace WebAPIApplication.Models
{
    public class CommentsRepository: BaseRepository
    {
        public CommentsRepository(string databaseUrl, string databaseName): base (databaseUrl, databaseName){

        }

        public IEnumerable<BlogComment> GetCommentsForPost(ObjectId post)
        {
            var Query = Query<BlogComment>.EQ(comment => comment.Post, post);
            return db.GetCollection<BlogComment>("comments").Find(Query).SetSortOrder(SortBy.Ascending("date"));;
        }

        public BlogComment Create(BlogComment comment){
            db.GetCollection<BlogComment>("comments").Save(comment);
            return comment;
        }
     }
}

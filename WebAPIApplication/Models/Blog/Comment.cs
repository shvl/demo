using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebAPIApplication.Models {
    public class BlogComment {
        [BsonElement("_id")]
        public ObjectId Id { get; set; }

        [BsonElement("post")]
        public ObjectId Post { get; set; }

        [BsonElement("user")]
        public string User { get; set; }

        [BsonElement("date")]
        public DateTime Date { get; set; }

        [BsonElement("comment")]
        public string Comment { get; set; }

        public BlogComment(ObjectId post, string user, string comment){
            this.Id = new ObjectId();
            this.Post = post;
            this.User = user;
            this.Date = DateTime.Now;
            this.Comment = comment;
        }
    }
}
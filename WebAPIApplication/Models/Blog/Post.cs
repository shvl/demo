using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebAPIApplication.Models {
    public class BlogPost {
        [BsonElement("_id")]
        public ObjectId Id { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("body")]
        public string Body { get; set; }

        [BsonElement("slug")]
        public string Slug { get; set; }

        [BsonElement("image")]
        public string Image { get; set; }
        [BsonElement("excerpt")]
        public string excerpt { get; set; }

        [BsonElement("date")]
        public DateTime Date { get; set; }

        [BsonElement("views")]
        public int Views { get; set; }

        [BsonElement("user")]
        public string User { get; set; }

        public BlogPost(string user, PostBody PostBody){
            this.Title = PostBody.title;
            this.Body = PostBody.body;
            this.Slug = PostBody.slug;
            int len = PostBody.body.Length;
            if (len > 150)
                len = 150;
            this.excerpt = PostBody.body.Substring(0, len) + "...";
            this.Date = DateTime.Now;
            this.Views = 0;
            this.User = user; 
        }
    }
}
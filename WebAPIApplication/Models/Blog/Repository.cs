using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;

namespace WebAPIApplication.Models
{
    public class BaseRepository
    {
        public MongoClient mongoClient;
        public MongoServer mongoServer;
        public MongoDatabase db;
 
        public BaseRepository(string databaseUrl, string databaseName)
        {
            System.Console.WriteLine("Connecting to "+databaseUrl);
            this.mongoClient = new MongoClient(databaseUrl);
            this.mongoServer = mongoClient.GetServer();
            this.db = mongoServer.GetDatabase(databaseName);      
        }
    }
}

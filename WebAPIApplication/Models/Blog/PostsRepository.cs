using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
 
namespace WebAPIApplication.Models
{
    public class PostsRepository : BaseRepository 
    {
        public PostsRepository(string databaseUrl, string databaseName): base (databaseUrl, databaseName){

        }
        
        public IEnumerable<BlogPost> GetPosts()
        {
            return this.db.GetCollection<BlogPost>("posts").FindAll().SetSortOrder(SortBy.Descending("date"));
        }
 
        public BlogPost GetPostBySlug(string slug)
        {
            var res = Query<BlogPost>.EQ( post => post.Slug, slug);
            return db.GetCollection<BlogPost>("posts").FindOne(res);
        }
 
        public BlogPost Create(BlogPost p)
        {
            db.GetCollection<BlogPost>("posts").Save(p);
            return p;
        }
 
        public void Update(ObjectId id, BlogPost p)
        {
            p.Id = id;
            var res = Query<BlogPost>.EQ(pd => pd.Id,id);
            var operation = Update<BlogPost>.Replace(p);
            db.GetCollection<BlogPost>("posts").Update(res,operation);
        }
        public void Remove(ObjectId id)
        {
            var res = Query<BlogPost>.EQ(e => e.Id, id);
            var operation = db.GetCollection<BlogPost>("BlogPost").Remove(res);
        }
    }
}

using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WebAPIApplication.Models {
    public class UserName{
        public string familyName { get; set; }
        public string givenName { get; set; }
    }
    public class UserData {
        [JsonProperty("name")]
        public UserName Name { get; set; }
    }
}
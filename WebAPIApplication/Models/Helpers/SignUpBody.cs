namespace WebAPIApplication.Models {
        public class SignUpBody{
        public string email { get; set; }
        public string familyName { get; set; }
        public string givenName { get; set; }
        public string password { get; set; }
    }
}
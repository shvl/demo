namespace WebAPIApplication.Models {

    public abstract class ResponseBase {} 
    public class Token: ResponseBase
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string jti { get; set; }
        public string scope { get; set; }
        public string token_type { get; set; }
    }
}

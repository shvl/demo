namespace WebAPIApplication.Models {
    public class PostBody{
        public string body { get; set; }
        public string slug { get; set; }
        public string title { get; set; }
    }
}
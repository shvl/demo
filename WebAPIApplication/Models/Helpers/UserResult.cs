
using System.Net;

namespace WebAPIApplication.Models {
    public class UserResult {
        public bool success;
        public User user;
        public HttpStatusCode error;

        public UserResult(User user){
            this.success = true;
            this.user = user;
        }

        public UserResult(HttpStatusCode error){
            this.success = false;
            this.error = error;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;

namespace WebAPIApplication.Models {

    public class IdentityProvider{
        private IdentityConfig identityConfig;

        public IdentityProvider(IdentityConfig identityConfig){
            this.identityConfig = identityConfig;
        }

        public async Task<Identity> getIdentity(){
            var identity = new Identity(identityConfig.Client, identityConfig.Secret, identityConfig.Url);
            await identity.getClientCredentials();
            return identity;
        }
    }

}

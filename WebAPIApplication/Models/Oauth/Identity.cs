using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;

namespace WebAPIApplication.Models {
    public class Identity {
        private string client;
        private string secret;
        private Token clientCredentials;
        private HttpClient httpClient;
        public Identity(string client, string secret, string baseUrl){
            this.client = client;
            this.secret = secret;
            this.httpClient= new HttpClient {
                BaseAddress = new Uri(baseUrl)
            };
        }

        public async Task getClientCredentials (){
            var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("grant_type", "client_credentials"),
                new KeyValuePair<string, string>("client_id", this.client),
                new KeyValuePair<string, string>("client_secret", this.secret),
            });
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", getBasicToken());

            var response = await httpClient.PostAsync("/oauth/token", content);
            response.EnsureSuccessStatusCode(); // Throw in not success

            string stringResponse = await response.Content.ReadAsStringAsync();
            this.clientCredentials = JsonConvert.DeserializeObject<Token>(stringResponse);
        }

        public async Task<UserResult> signIn(Credentials credentials){
            var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("username", credentials.email),
                new KeyValuePair<string, string>("password", credentials.password),
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("response_type", "token"),
            });

            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", getBasicToken());

            var response = await httpClient.PostAsync("/oauth/token", content);
            if (response.StatusCode != HttpStatusCode.OK)
                return new UserResult(response.StatusCode);

            string stringResponse = await response.Content.ReadAsStringAsync();
            Token token = JsonConvert.DeserializeObject<Token>(stringResponse);
            User user = await this.checkToken(token.access_token);
            await this.updateUser(user);
            return new UserResult(user);
        }

        public async Task<User> updateUser(User user){
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", this.clientCredentials.access_token);

            var response = await httpClient.GetAsync("/Users/"+user.user_id+"/verify");
            string stringResponse = await response.Content.ReadAsStringAsync();
            UserData userData = JsonConvert.DeserializeObject<UserData>(stringResponse);
            user.family_name = userData.Name.familyName;
            user.given_name = userData.Name.givenName;
            return user;
        }

        public async Task<UserResult> signUp(SignUpBody signUpBody){
            dynamic name = new JObject();
            name.Add("givenName", signUpBody.givenName);
            name.Add("familyName", signUpBody.familyName);
            dynamic email = new JObject();
            email.Add("value", signUpBody.email.ToLower());
            JArray emails = new JArray();
            emails.Add(email);

            dynamic jsonObject = new JObject();
            jsonObject.Add("userName", signUpBody.email.ToLower());
            jsonObject.Add("password", signUpBody.password);
            jsonObject.Add("name", name);
            jsonObject.Add("emails", emails);

            var content = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", this.clientCredentials.access_token);

            var response = await httpClient.PostAsync("/Users", content);
            if (response.StatusCode != HttpStatusCode.Created)
                return new UserResult(response.StatusCode);

            string stringResponse = await response.Content.ReadAsStringAsync();
            return await this.signIn(new Credentials(signUpBody.email, signUpBody.password));
        }

        public async Task<User> checkToken (string token){
            var content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("token", token),
            });

            var byteArray = Encoding.ASCII.GetBytes(this.client+":"+this.secret);
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", getBasicToken());

            var response = await httpClient.PostAsync("/check_token", content);
            response.EnsureSuccessStatusCode(); 

            string stringResponse = await response.Content.ReadAsStringAsync();
            User user = JsonConvert.DeserializeObject<User>(stringResponse);
            user.access_token = token;
            return user;
        }

        private string getBasicToken(){
            var byteArray = Encoding.ASCII.GetBytes(this.client+":"+this.secret);
            return Convert.ToBase64String(byteArray);
        }

    }
}

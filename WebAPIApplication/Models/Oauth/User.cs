namespace WebAPIApplication.Models {
    public class User{
        public string user_id { get; set; }
        public string email { get; set; }
        public string family_name { get; set; }
        public string given_name { get; set; }
        public string access_token { get; set; }
    }
}

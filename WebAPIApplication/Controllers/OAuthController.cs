using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Mvc;
using WebAPIApplication.Models;
using Microsoft.Extensions.Options;

namespace WebAPIApplication.Controllers
{
    [Route("api/user")]
    public class OAuthController : Controller
    {

        IdentityProvider identityProvider;
        public OAuthController(IOptions<IdentityConfig> IdentityConfig){
            this.identityProvider = new IdentityProvider(IdentityConfig.Value);
        }

        [HttpPost("sign-in")]
        public async Task<IActionResult> SignIn([FromBody]Credentials credentials)
        {
            var identity = await this.identityProvider.getIdentity();
            UserResult result = await identity.signIn(credentials);
            if (!result.success)
                return Unauthorized();
            return new ObjectResult(result.user);
        }

        [HttpPost("sign-up")]
        public async Task<IActionResult> SignUp([FromBody]SignUpBody signUpBody)
        {
            var identity = await this.identityProvider.getIdentity();
            UserResult result = await identity.signUp(signUpBody);
            if (!result.success){
                var res = new ObjectResult(result.error);
                res.StatusCode = (int) result.error;
                return res;
            }
            return new ObjectResult(result.user);
        }

        [HttpPost("checkToken")]
        public async Task<IActionResult> checkToken([FromBody]string token)
        {
            var identity = await this.identityProvider.getIdentity();
            User user = await identity.checkToken(token);
            if (user == null)
                return Unauthorized();
            return new ObjectResult(user);
        }

    }
}

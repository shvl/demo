using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Mvc;
using WebAPIApplication.Models;
using Microsoft.Extensions.Options;
using System.Text.RegularExpressions;


namespace WebAPIApplication.Controllers
{
    [Route("api/blog")]
    public class BlogController : Controller
    {
        PostsRepository postsRepository;
        CommentsRepository commentsRepository;
        IdentityProvider identityProvider;
        private string GenerateSlug(string phrase) 
        { 
            string str = phrase.ToLower(); 
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", ""); 
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim(); 
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();   
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str; 
        } 


        public BlogController(IOptions<BlogConfig> blogConfig, IOptions<IdentityConfig> IdentityConfig){
            string dbUrl = blogConfig.Value.DatabaseUrl;
            string dbName = blogConfig.Value.DatabaseName;
            postsRepository = new PostsRepository(dbUrl, dbName);
            commentsRepository = new CommentsRepository(dbUrl, dbName);
            this.identityProvider = new IdentityProvider(IdentityConfig.Value);
        }

        [HttpGet("posts")]
        public IEnumerable<BlogPost> GetPostsList()
        {
            return postsRepository.GetPosts();
        }

        [HttpPost("posts")]
        public async Task<IActionResult> CreatePosts([FromBody] PostBody postBody)
        {
            string token;
            try
            {
                token = Request.Headers["Authorization"][0].Split(' ')[1];
            }
            catch (System.Exception)
            {
                return Unauthorized();
            }
            var identity = await this.identityProvider.getIdentity();
            var user = await identity.checkToken(token);
            if (user == null)
                return Unauthorized();

            string slug = this.GenerateSlug(postBody.title);
            var post = postsRepository.GetPostBySlug(slug);
            if (post != null){
                slug = slug + "-"+ GenerateSlug(DateTime.Now.ToFileTimeUtc().ToString());
            }
            postBody.slug = slug;
            var newPost = new BlogPost(user.email, postBody);
            var result = postsRepository.Create(newPost);
            return new ObjectResult(result);
        }

        [HttpGet("posts/{slug}")]
        public BlogPost GetPostBySlug(string slug)
        {
            return postsRepository.GetPostBySlug(slug);
        }

        [HttpGet("posts/{slug}/comments")]
        public IEnumerable<BlogComment> GetComments(string slug)
        {
            var post = postsRepository.GetPostBySlug(slug);
            return commentsRepository.GetCommentsForPost(post.Id);
        }

        [HttpPost("posts/{slug}/comments")]
        public async Task<IActionResult> CreateComment(string slug, [FromBody]CommentBody comment)
        {
            string token;
            try
            {
                token = Request.Headers["Authorization"][0].Split(' ')[1];
            }
            catch (System.Exception)
            {
                return Unauthorized();
            }

            var identity = await this.identityProvider.getIdentity();
            var user = await identity.checkToken(token);
            if (user == null)
                return Unauthorized();

            var post = postsRepository.GetPostBySlug(slug);
            if (post == null)
                return NotFound();

            return new ObjectResult(commentsRepository.Create(new BlogComment(post.Id, user.email, comment.comment)));
        }
    }
}
